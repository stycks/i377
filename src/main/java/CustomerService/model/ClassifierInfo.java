package CustomerService.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author erika
 */
@Getter
@Setter
@AllArgsConstructor
public class ClassifierInfo {

  private List<String> phoneTypes;
  private List<String> customerTypes;
}
