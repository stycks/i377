package CustomerService.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * @author erika
 */
@Data
@Entity
@Table(name = "classifier_type")
public class ClassifierType {

  @Id
  private Long id;

  @Column(name = "classifier_name")
  private String classifierName;

  @Column(name = "classifier_value_name")
  private String classifierValueName;
}
