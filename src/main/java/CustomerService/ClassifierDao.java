package CustomerService;

import CustomerService.Util.DataSourceProvider;
import CustomerService.Util.JpaUtil;
import CustomerService.model.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * @author erika
 */
public class ClassifierDao {

  public List<String> findClassifierValues(String type) {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      TypedQuery<String> query = em.createQuery("SELECT c.classifierValueName FROM ClassifierType c WHERE c.classifierName = ?1", String.class);
      query.setParameter(1, type);
      return query.getResultList();
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }
}
