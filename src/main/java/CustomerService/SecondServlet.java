package CustomerService;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CustomerService.model.Person;

/**
 * @author erika
 */
@WebServlet("/api/customers")
public class SecondServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  PersonDao personDao = new PersonDao();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Person person = new Person();
    person.setType("customer_type.corporate");
    personDao.addPerson(person);
    String id = req.getParameter("id");
    resp.setContentType("application/json");
    if (id != null) {
      new ObjectMapper().writeValue(resp.getOutputStream(), personDao.getPerson(id));
    }
    else {
      new ObjectMapper().writeValue(resp.getOutputStream(), personDao.getPeople());
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Person person = new ObjectMapper().readValue(req.getInputStream(), Person.class);
    personDao.addPerson(person);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String id = req.getParameter("id");
    if (id != null) {
      personDao.deletePerson(id);
    }
    else {
      personDao.deleteAll();
    }
  }
}
