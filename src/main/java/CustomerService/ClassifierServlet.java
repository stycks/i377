package CustomerService;

import CustomerService.model.ClassifierInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author erika
 */
@WebServlet("/api/classifiers")
public class ClassifierServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  ClassifierDao classifierDao = new ClassifierDao();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("application/json");

    ClassifierInfo classifierInfo = new ClassifierInfo(
            classifierDao.findClassifierValues("phone_type"),
            classifierDao.findClassifierValues("customer_type"));
    new ObjectMapper().writeValue(resp.getOutputStream(), classifierInfo);
  }
}
