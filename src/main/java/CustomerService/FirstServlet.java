package CustomerService;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CustomerService.model.Person;

/**
 * @author erika
 */
@WebServlet("/customers/form")
public class FirstServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  PersonDao personDao = new PersonDao();

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Person person = new Person();
    person.setFirstName(req.getParameter("name"));
    personDao.addPerson(person);
  }
}
