package CustomerService;

import CustomerService.Util.JpaUtil;
import CustomerService.model.Person;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * @author erika
 */
public class PersonDao {

  public List<Person> getPeople() {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      TypedQuery<Person> query = em.createQuery("SELECT DISTINCT p FROM Person p left join fetch p.phones", Person.class);
      return query.getResultList();
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }

  public Person getPerson(String id) {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      TypedQuery<Person> query = em.createQuery("SELECT DISTINCT p FROM Person p left join fetch p.phones WHERE id = :id", Person.class);
      query.setParameter("id", Long.parseLong(id, 10));
      List<Person> optionalPerson = query.getResultList();
      Person person = (optionalPerson.isEmpty()) ? null : optionalPerson.get(0);
      person.setFirstName("Jyri");
      em.getTransaction().begin();
      em.persist(person);
      em.getTransaction().commit();
      TypedQuery<Person> query2 = em.createQuery("SELECT DISTINCT p FROM Person p left join fetch p.phones WHERE id = :id", Person.class);
      query2.setParameter("id", Long.parseLong(id, 10));
      List<Person> optionalPerson2 = query2.getResultList();
      return (optionalPerson.isEmpty()) ? null : optionalPerson.get(0);
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }

  public void addPerson(Person person) {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      em.getTransaction().begin();
      em.persist(person);
      em.getTransaction().commit();
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }

  public void deletePerson(String id) {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      TypedQuery<Person> query = em.createQuery("SELECT DISTINCT p FROM Person p left join fetch p.phones WHERE id = :id", Person.class);
      query.setParameter("id", Long.parseLong(id, 10));
      List<Person> optionalPerson = query.getResultList();
      Person person = (optionalPerson.isEmpty()) ? null : optionalPerson.get(0);
      em.getTransaction().begin();
      em.remove(person);
      em.getTransaction().commit();
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }

  public void deleteAll() {
    EntityManager em = null;
    try {
      em = JpaUtil.getFactory().createEntityManager();
      em.getTransaction().begin();
      TypedQuery<Person> query = em.createQuery("SELECT DISTINCT p FROM Person p left join fetch p.phones", Person.class);
      List<Person> people = query.getResultList();
      for (int i = 0; i < people.size(); i++) {
        em.remove(people.get(i));
      }
      em.getTransaction().commit();
    } finally {
      JpaUtil.closeQuietly(em);
    }
  }
}
