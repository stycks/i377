package CustomerService;

import CustomerService.Util.DataSourceProvider;
import CustomerService.Util.FileUtil;
import CustomerService.Util.PropertyLoader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

/**
 * @author erika
 */
 @WebListener
public class Listener implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    DataSourceProvider.setDbUrl(PropertyLoader.getProperty("javax.persistence.jdbc.url"));

    DataSource dataSource = DataSourceProvider.getDataSource();
    String schema = FileUtil.readFileFromClasspath("schema.sql");

    try (Connection conn = dataSource.getConnection()) {
      try (Statement statement = conn.createStatement()) {
        List<String> commands = Stream.of(schema.split(";"))
                .filter(line -> !line.trim().isEmpty())
                .collect(Collectors.toList());
        for (String sql : commands) {
          statement.executeUpdate(sql);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContextEvent) {

  }
}
